<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="jumbotron">
                    <h1>eBanking Web Application</h1>
                </div>
            </div>
            <div class="row">

                <!-- Login panel -->
                <div class="col-sm-6 col-md-7">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <h1>Login</h1>
                        </div>

                        <div class="panel-body">

                            <?php echo form_open('auth_controller/login_process'); ?>
                            <div class="form-group">
                                <label for="uname">Username:</label>
                                <input type="text" id="uname" name="username" class="form-control" placeholder="Enter your username..." required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" id="pwd" name="password" class="form-control" placeholder="Enter your password..." required>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
                <!-- Login panel END -->

                <!-- Registration panel -->
                <div class="col-sm-6 col-md-5">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            <h1>...or Register a new account</h1>
                        </div>
                        <div class="panel-body">

                            <?php echo form_open('auth_controller/registration_process'); ?>
                            <div class="form-group">
                                <label for="name">Your full name:</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Enter your full name..." required autofocus>
                            </div>

                            <div class="form-group">
                                <label for="tel">Your telephone number:</label>
                                <input type="number" id="tel" name="tel" class="form-control" placeholder="Enter your telephone number..." required>
                            </div>

                            <div class="form-group">
                                <label for="uname">Username:</label>
                                <input type="text" id="uname" name="username" class="form-control" placeholder="Enter your username..." required>
                            </div>

                            <div class="form-group">
                                <label for="pwd">Password:</label>
                                <input type="password" id="pwd" name="password" class="form-control" placeholder="Enter your password..." required>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
                            </div>

                            </form>
                        </div>
                    </div>
                </div>
                <!-- Registration panel END -->

            </div>
        </div>
    </div>
</body>
</html>
