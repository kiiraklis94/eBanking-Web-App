<?php

class Auth_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Auth_Model');
        $this->load->helper('url_helper');
        $this->load->library('form_validation');
        $this->load->library('session');
    }

    public function login() {
        $this->load->view('auth/login_page');
    }

    public function login_process() {
        $this->load->helper(array('form', 'url'));

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo "wrong";
        } else {
            $uname = $this->input->post('username');
            $pwd = $this->input->post('password');

            $loginDetails = ["username" => "$uname", "password" => "$pwd"];

            if ($this->Auth_Model->login($loginDetails)) {

                echo "logged in as: ";

                $userData = $this->Auth_Model->get_user_data($uname);

                foreach ($userData as $value) {
                    $this->session->set_userdata('name', $value['customer_name']);
                    $this->session->set_userdata('username', $value['username']);
                }
                echo $this->session->username . "<br>";
            } else {
                
            }
        }
    }

    public function registration_process() {
        
    }

}
