<?php

class Auth_Model extends CI_Model {

    public function __construct() {
        $this->load->database();
    }

    public function login($loginDetails) {
        $condition = "username =" . "'" . $loginDetails['username'] . "' AND " . "password =" . "'" . $loginDetails['password'] . "'";

        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return true;
        } else {
            $this->db->where('username', $loginDetails['username']);
            $this->db->set('accountLock_count', 'accountLock_count + 1', FALSE);
            $this->db->update('customers');
            return false;
        }
    }

    public function get_user_data($username) {
        $condition = "username =" . "'" . $username . "'";

        $this->db->select('*');
        $this->db->from('customers');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result_array();
        }
        else{
            echo "Error retrieving user data";
        }
    }

}
